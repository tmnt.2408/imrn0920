console.log("");
console.log("if-else");
console.log("");

var nama = "";
var peran = "";
var namaLength = nama.length;
var peranLength = peran.length;

if ( namaLength == 0 && peranLength == 0 ) {
    console.log("Nama harus diisi!");
};

console.log("");

var nama2 = "John";
var peran2 = "";
var namaLength2 = nama2.length;
var peranLength2 = peran2.length;

if ( namaLength2 == 0 && peranLength2 == 0 ) {
    console.log("Nama harus diisi!");
} else if ( namaLength2 > 0 && peranLength2 == 0 ) {
    console.log("Halo " + nama2 + ", Pilih peranmu untuk memulai game!");
};

console.log("");

var nama3 = "Jane";
var peran3 = "Penyihir";
var namaLength3 = nama3.length;
var peranLength3 = peran3.length;

if ( namaLength3 == 0 && peranLength3 == 0 ) {
    console.log("Nama harus diisi!");
} else if ( namaLength3 > 0 && peranLength3 == 0 ) {
    console.log("Halo " + nama3 + ", Pilih peranmu untuk memulai game!");
} else if ( namaLength3 > 0 && peran3 == "Penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama3);
    console.log("Halo " + peran3 + " " + nama3 + ", kamu dapat melihat siapa yang menjadi werewolf!");
};

console.log("");

var nama4 = "Jenita";
var peran4 = "Guard";
var namaLength4 = nama4.length;
var peranLength4 = peran4.length;

if ( namaLength4 == 0 && peranLength4 == 0 ) {
    console.log("Nama harus diisi!");
} else if ( namaLength4 > 0 && peranLength4 == 0 ) {
    console.log("Halo " + nama4 + ", Pilih peranmu untuk memulai game!");
} else if ( namaLength4 > 0 && peran4 == "Penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama4);
    console.log("Halo " + peran4 + " " + nama4 + ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if ( namaLength4 > 0 && peran4 == "Guard" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama4);
    console.log("Halo " + peran4 + " " + nama4 + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
};

console.log("");

var nama5 = "Jenita";
var peran5 = "Guard";
var namaLength5 = nama5.length;
var peranLength5 = peran5.length;

if ( namaLength5 == 0 && peranLength5 == 0 ) {
    console.log("Nama harus diisi!");
} else if ( namaLength5 > 0 && peranLength5 == 0 ) {
    console.log("Halo " + nama5 + ", Pilih peranmu untuk memulai game!");
} else if ( namaLength5 > 0 && peran5 == "Penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama5);
    console.log("Halo " + peran5 + " " + nama5 + ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if ( namaLength5 > 0 && peran5 == "Guard" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama5);
    console.log("Halo " + peran5 + " " + nama5 + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if ( namaLength5 > 0 && peran5 == "Werewolf" ) {
    console.log("Selamat datang di Dunia Werewolf, " + nama5);
    console.log("Halo " + peran5 + " " + nama5 + ", kamu akan memakan mangsa setiap malam!");
};

console.log("");
console.log("switch");
console.log("");

var tanggal = 21;
var bulan = 2;
var tahun = 1975;

if ( tanggal < 1 || tanggal > 31 ) {
    console.log("Masukkan tanggal antara 1 - 31");
} else if ( bulan < 1 || bulan > 12 ) {
    console.log("Masukkan bulan antara 1 - 12");
} else if ( tahun < 1900 || bulan > 2200 ) {
    console.log("Masukkan tahun antara 1900 - 2200");
} else {
    switch (bulan) {
        case 1 : console.log(tanggal + " Januari " + tahun); break;
        case 2 : console.log(tanggal + " Februari " + tahun); break;
        case 3 : console.log(tanggal + " Maret " + tahun); break;
        case 4 : console.log(tanggal + " April " + tahun); break;
        case 5 : console.log(tanggal + " Mei " + tahun); break;
        case 6 : console.log(tanggal + " Juni " + tahun); break;
        case 7 : console.log(tanggal + " Juli " + tahun); break;
        case 8 : console.log(tanggal + " Agustus " + tahun); break;
        case 9 : console.log(tanggal + " September " + tahun); break;
        case 10 : console.log(tanggal + " Oktober " + tahun); break;
        case 11 : console.log(tanggal + " November " + tahun); break;
        case 12 : console.log(tanggal + " Desember " + tahun); break;
    }
};

console.log("");