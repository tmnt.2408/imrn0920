console.log("");
console.log("No. 1 Looping While");
console.log("");
console.log("LOOPING PERTAMA");
var i = 2;
var j = 0;
while(j < 20) {
  j += i;
  console.log(j + " - I love coding");
};
console.log("LOOPING KEDUA");
var i = 2;
var j = 22;
while(j > 2) {
  j -= i;
  console.log(j + " - I will become a mobile developer");
};
console.log("");
console.log("No. 2 Looping For");
console.log("");
for(var i = 1; i < 21; i++) {
  if ( i%2 != 0 && i%3 != 0 ) {
    console.log(i + " - Santai");
  } else if ( i%2 == 0 ) {
    console.log(i + " - Berkualitas");
  } else if ( i%2 != 0 && i%3 == 0 ) {
    console.log(i + " - I Love Coding");  
  };
};
console.log("");
console.log("No. 3 Membuat Persegi Panjang #");
console.log("");
var baris = 8;
var kolom = 4;
for (var i = 0; i < kolom; i++) {
  console.log("#".repeat(baris));
};
console.log("");
console.log("No. 4 Membuat Tangga #");
console.log("");
var baris = 1;
var kolom = 7;
for (var i = 0; i < kolom; i++) {
  console.log("#".repeat(baris++));
};
console.log("");
console.log("No. 5 Membuat Papan Catur #");
console.log("");
var baris = 4;
var kolom = 8;
for (var i = 0; i < kolom; i++) {
  if ( i%2 != 0) {
    console.log("# ".repeat(baris));
  } else if ( i%2 == 0 ) {
    console.log(" #".repeat(baris));
  };
};