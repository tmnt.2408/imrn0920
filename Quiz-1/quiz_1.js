console.log("");
console.log("Soal Quiz-1 Nomor 9");
console.log("");
function myApp(){
    var total = 5;
    var output = "";
    for (var i = 1; i <= total; i++) {
        for(var j = 1; j <= i; j++) {
            output += j + "";
        };
        console.log(output);
        output = "";
    };
};
myApp();
console.log("");
console.log("Jawaban Quiz-1 Nomor 13");
console.log("");
function DescendingTen(value1) {
    var j = [];
    if (!value1) {
        return -1
    } else {
        for (var i = 0; i < 10; i++) {
            j.push(value1 - i);
        };
    };
    return j;
};
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1
console.log("");
console.log("Jawaban Quiz-1 Nomor 14");
console.log("");
function ularTangga(){
    var l = [];
    var k = [];
    var j = true;
    for (var g = 10; g >= 1; g--) {
      for (let i = 1; i <= 10; i++) {
        j ?
          k.push( 10 * ( g - 1 ) + ( 11 - i )) : 
          k.push( 10 * ( g - 1 ) + i );
        };
      j = !j;
      l.push(k);
      k = [];
    };
    return l;
};
console.log(ularTangga());
console.log("");
console.log("Jawaban Quiz-1 Nomor 15");
console.log("");
function balikString(kata) {
    var j = " ";
    for (var i = kata.length - 1; i >= 0; i--) {
        j += kata[i];
    };
    return j;
};
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log("");
console.log("Jawaban Quiz-1 Nomor 16 #1");
console.log("");
function palindrome1(str) {
    return str == str.split('').reverse().join('');
};
console.log(palindrome1("kasur rusak")) // true
console.log(palindrome1("haji ijah")) // true
console.log(palindrome1("nabasan")) // false
console.log(palindrome1("nababan")) // true
console.log(palindrome1("jakarta")) // false
console.log("");
console.log("Jawaban Quiz-1 Nomor 16 #2");
console.log("");
function palindrome2(value1) {
    var j = Math.floor(value1.length / 2);
    for (var i = 0; i < j; i++) {
        if (value1[i] !== value1[value1.length - i - 1]) {
            return false;
        };
    };
    return true;
};
console.log(palindrome2("kasur rusak")) // true
console.log(palindrome2("haji ijah")) // true
console.log(palindrome2("nabasan")) // false
console.log(palindrome2("nababan")) // true
console.log(palindrome2("jakarta")) // false