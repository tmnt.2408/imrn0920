console.log("");
console.log("Soal 1");
console.log("");
function arrayToObject(arr) {
    if (arr.length > 0) {
        for (var i=0; i < arr.length; i++) {
            var arrObject = {};
            var now = new Date();
            var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
            var birthYear = arr[i][3];
            var age;
            if (birthYear > thisYear || !birthYear) {
                age = "invalid Birth Year";
            } else {
                age = thisYear - birthYear;
            };
            arrObject.firstName = arr[i][0];
            arrObject.lastName = arr[i][1];
            arrObject.gender= arr[i][2];
            arrObject.age = age;
            var textObject = (i + 1) + '. ' + arrObject.firstName + ' ' + arrObject.lastName + ' : ';
            console.log(textObject);
            console.log(arrObject);
        };
    };
};
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]) // ""
console.log("");
console.log("Soal 2");
console.log("");
function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        var arrObject ={}
        var listPurchased = [];
        var changeMoney = money;
        var isValid = true;
        while (changeMoney > 0 && isValid) {
            if (changeMoney >= 1500000) {
                listPurchased.push('Sepatu Stacattu');
                changeMoney -= 1500000;
                isValid = true;
            };
            if (changeMoney >= 500000) {
                listPurchased.push('Baju Zoro');
                changeMoney -= 500000;
                isValid = true;
            };
            if (changeMoney >= 250000) {
                listPurchased.push('Baju H&N');
                changeMoney -= 250000;
                isValid = true;
            };
            if (changeMoney >= 175000) {
                listPurchased.push('Sweater Uniklooh');
                changeMoney -= 175000;
                isValid = true;
            };
            if (changeMoney >= 50000) {
                listPurchased.push('Casing Handphone');
                changeMoney -= 50000;
                isValid = true;
            };
            isValid = false;
        };
        arrObject.memberId = memberId;
        arrObject.money = money;
        arrObject.listPurchased = listPurchased;
        arrObject.changeMoney = changeMoney;
        return arrObject;
    };
};
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("");
console.log("Soal 3");
console.log("");
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var initialObject, bayar, naikDari, tujuan, finalObject = [];
    for (var i = 0; i < arrPenumpang.length; i++) {
        initialObject = new Object();
        bayar = 0
        initialObject.penumpang = arrPenumpang[i][0];
        initialObject.naikDari = arrPenumpang[i][1];
        initialObject.tujuan = arrPenumpang[i][2];
        for (var j = 0; j < rute.length; j++) {
            if (rute[j] === arrPenumpang[i][1]) {
                naikDari = j;
            };
            if (rute[j] === arrPenumpang[i][2]) {
                tujuan = j;
            };
        };
        initialObject.bayar = Math.abs(naikDari - tujuan) * 2000;
        finalObject.push(initialObject);
    };
    return finalObject;
};
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]