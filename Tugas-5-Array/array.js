console.log("");
console.log("No. 1");
console.log("");

function range(startNum, finishNum) {
    var k = [];
    if (startNum < finishNum) {
        var j = finishNum - startNum + 1;
        for (var i = 0; i < j; i++) {
            k.push(startNum + i);
        };
    } else if (startNum > finishNum) {
        var j = startNum - finishNum + 1;
        for (var i = 0; i < j; i++) {
            k.push(startNum - i);
        };
    } else {
        return -1;
    };
    return k;
};

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 

console.log("");
console.log("No. 2");
console.log("");

function rangeWithStep(startNum, finishNum, step) {
    var k = [];
    if (startNum < finishNum) {
        var j = startNum;
        for (var i = 0; j <= finishNum; i++) {
            k.push(j);
            j += step;
        };
    } else if (startNum > finishNum) {
        var j = startNum;
        for (var i = 0; j >= finishNum; i++) {
            k.push(j);
            j -= step;
        };
    } else {
        return -1;
    };
    return k;
};

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 

console.log("");
console.log("No. 3");
console.log("");

function sum(startNum, finishNum, step) {
    var k = [];
    var l;
    if (!step) {
        l = 1;
    } else {
        l = step;
    };
    if (startNum < finishNum) {
        var j = startNum;
        for (var i = 0; j <= finishNum; i++) {
            k.push(j);
            j += l;
        }
    } else if(startNum > finishNum) {
        var j = startNum;
        for (var i = 0; j >= finishNum; i++) {
            k.push(j);
            j -= l;
        };
    } else if (startNum) {
        return startNum;
    } else {
        return 0;
    }
    var t = 0;
    for (var i = 0; i < k.length; i++) {
        t = t + k[i];
    };
    return t;
};

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("");
console.log("No. 4");
console.log("");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(data) {
    var j = data.length;
    for (var i = 0; i < j; i++) {
        var a = "Nomor Id : " + data[i][0];
        var b = "Nama Lengkap : " + data[i][1];
        var c = "TTL : " + data[i][2] + ", " + data[i][3];
        var d = "Hobi : " + data[i][4];
        console.log("");
        console.log(a);
        console.log(b);
        console.log(c);
        console.log(d);
        console.log("");
    };
};

dataHandling(input);

console.log("");
console.log("No. 5");
console.log("");

function balikKata(kata) {
    var j = " ";
    for (var i = kata.length - 1; i >= 0; i--) {
        j += kata[i];
    };
    return j;
};

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("");
console.log("No. 6");
console.log("");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data) {
    var a = data;
    var b = data[1] + "Elsharawy";
    var c = "Provinsi " + data[2];
    var d = "Pria";
    var e = "SMA International Metro";
    a.splice(1, 1, b);
    a.splice(2, 1, c);
    a.splice(4, 1, d, e);

    var f = data[3];
    var g = f.split("/");
    var h = g[1];
    var i = " ";
    switch (h) {
        case "01":
            i = "Januari";
            break;
        case "02":
            i = "Februari";
            break;
        case "03":
            i = "Maret";
            break;
        case "04":
            i = "April";
            break;
        case "05":
            i = "Mei";
            break;
        case "06":
            i = "Juni";
            break;
        case "07":
            i = "Juli";
            break;
        case "08":
            i = "Agustus";
            break;
        case "09":
            i = "September";
            break;
        case "10":
            i = "Oktober";
            break;
        case "11":
            i = "November";
            break;
        case "12":
            i = "Desember";
            break;
    };

    var j = g.join("-");
    var k = g.sort(function(value1, value2) {
                    return value2 - value1 });
    var m = b.slice(0, 15);
    console.log(a);
    console.log(i);
    console.log(k);
    console.log(j);
    console.log(m);
};

console.log("");