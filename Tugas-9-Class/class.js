console.log("");
console.log("Nomor 1 Release 0");
console.log("");
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    };
    get name() {
        return this._name;
    };
    set name(x) {
        this._name = x;
    };
    get legs() {
        return this._legs;
    };
    set legs(x) {
        this._legs = x;
    };
    get cold_blooded() {
        return this._cold_blooded;
    };
    set cold_blooded(x) {
        this._cold_blooded = x;
    };
}; 
var sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false
console.log("");
console.log("Nomor 1 Release 1");
console.log("");
class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    };
    yell() {
    return console.log("Auooo");
    };
};
class Frog extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    };
    jump() {
        return console.log("hop hop");
    }
};
var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"
console.log("");
console.log("Nomor 2");
console.log("");
class Clock {
    constructor({ template }) {
      this.template = template;
    };
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    };
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    };
  };
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 