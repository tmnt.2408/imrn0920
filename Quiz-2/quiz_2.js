class Score {
    // Code disini
    constructor(subject, email, points){
        this.subject = subject
        this.email = email
        this.points = points
    }

    average () {
        let totalPoint = 0;
        for (let i = 0; i< this.points.length; i++){
            totalPoint += this.points[i]
        }
        return (totalPoint / this.points.length).toFixed(1)
    }
  }

  const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    // code kamu di sini

    let scoreArr = []
    let subjectIndex;
    switch (subject){
        case "quiz-1":
            subjectIndex = 1
            break;
        case "quiz-2":
            subjectIndex= 2
            break;
        case "quiz-3":
            subjectIndex = 3
            break;
        default:
            break;
    }
    for(let i = 1; i<data.length; i++){
        const score = new Score (subject, data[i][0], data[i][subjectIndex] )
        scoreArr.push(score)
    }
    console.log(scoreArr)
  }
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")

  function recapScores(data) {
    // code kamu di sini
    let recapArr = []
    for (let i = 1; i<data.length; i++){
        const [email, ...nilai] = data [i]
        const score = new Score("Recap", data[i][0], nilai)
        const rata2 = score.average()

        let predikat;
        if(rata2 > 90){
            predikat = "honour"
        }else if( rata2 > 80){
            predikat ="graduate"
        } else if( rata2 > 70){
            predikat = "participant"
        }

        const template = ` ${i}. Email: ${score.email} Rata-rata: ${rata2} predikat: ${predikat}`

        console.log(template)
    }
  }
  
  recapScores(data);