const daftarHobi = ['Olahraga', 'Makan', 'Ngoding'];
let hobiFavorit;
for (const hobi of daftarHobi) {
    hobiFavorit = hobi;
}
console.log(hobiFavorit);

// let sum = 0;
// for (const i=2; i<=6; i++) {
//     sum = sum + i;
// }
// console.log(sum);

const propKey = 'field 12';
const person = {
    [propKey]: 'Abduh'
};
console.log(person['field 12']);

if (5 + 2 === 7 && 4 > 5 || 'Hi' === 'Hi') {
    console.log("True");
} else {
    console.log("False");
};

// updateStatus(status) {
//     this.setState({status})
// };