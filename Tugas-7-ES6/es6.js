console.log("");
console.log("Soal 1");
console.log("");
const golden = goldenFunction = () => {
  console.log("this is golden!!");
};
golden();
console.log("");
console.log("Soal 2");
console.log("");
const newFunction = literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName() {
        console.log(firstName, lastName)
        return 
      }
    }
};
//Driver Code 
newFunction("William", "Imoh").fullName();
console.log("");
console.log("Soal 3");
console.log("");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};
const {firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)
console.log("");
console.log("Soal 4");
console.log("");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...east, ...west];
//Driver Code
console.log(combined);
console.log("");
console.log("Soal 5");
console.log("");
const planet = "earth";
const view = "glass";
let before = 'Lorem ' + `${view}` + 'dolor sit amet, ' +  
'consectetur adipiscing elit,' + `${planet}` + 'do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam';
// Driver Code
console.log(before);
console.log("");