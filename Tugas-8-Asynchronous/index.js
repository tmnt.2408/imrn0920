// di index.js
var readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 
let i = 0;
let j = 10000
function callReadBooks() {
    readBooks(j, books[i], function(timeLeft) {
        j = timeLeft;
        i++;
        if (i < books.length) {
            callReadBooks();
        };
    });
};
callReadBooks();