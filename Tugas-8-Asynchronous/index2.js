var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 
async function callReadBooksPromise() {
    let j = 10000
    for (let i = 0; i< books.length; i++){
        j = await readBooksPromise(j, books[i]).then(function(timeLeft) {
            return timeLeft;
        })
        .catch(function(timeLeft) {
            return timeLeft;
        })
    };
};
callReadBooksPromise();